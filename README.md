Скопируйте .env

```shell
cp .env.example .env
```

Ставим зависимости
```shell
composer install
```

Запускаем
```shell
php artisan serve
```

В браузере открываем http://127.0.0.1:8000

Запуск команд artisan
```shell
php artisan migrate 
# Миграции базы данных
php artisan db:seed --class=DepartmentSeeder
# сидер отделов
php artisan db:seed --class=EmployeeSeeder
# сидер сотрудников
php artisan db:seed --class=DepartmentEmployeeSeeder
# сидер связей отдлеов / сотрудников
```
