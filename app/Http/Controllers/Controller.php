<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(title="Laravel Swagger API", version="0.1")
     */

    /**
     *
     * @OA\Tag(
     *     name="Departments",
     *     description="All departmnets",
     * )
     * @OA\Server(
     *     description="Laravel Swagger API server",
     *     url="http://localhost/api"
     * )
     * @OA\SecurityScheme(
     *     type="apiKey",
     *     in="header",
     *     name="X-APP-ID",
     *     securityScheme="X-APP-ID"
     * )
     */
}
