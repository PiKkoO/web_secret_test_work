<?php

namespace App\Http\Controllers\Departments;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDepartmentRequest;
use App\Http\Resources\DepartmentCollection;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EditDepartmentResource;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    /**
     *
     * @OA\Get(
     *     path="/departments/all",
     *     operationId="departmentsAll",
     *     tags={"Departments"},
     *     @OA\Response(response="200", description="A department resource")
     * )
     *
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request  $request): Response
    {

        $departments = Department::orderBy('id', 'desc');
        $response = DepartmentResource::collection($departments->get());

        if($request->pageSize) {
            $departments = $departments->paginate($request->pageSize);
            $response = new DepartmentCollection($departments);
        }

        return response($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDepartmentRequest  $request
     * @return Response
     */
    public function store(StoreDepartmentRequest $request): Response
    {
        Department::create($request->all());
        return response([
            'message' => 'Department successfully added!',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id): Response
    {
        $department = Department::findOrFail($id);
        $response = new EditDepartmentResource($department);
        return response($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreDepartmentRequest $request
     * @param int $id
     * @return Response
     */
    public function update(StoreDepartmentRequest $request, int $id): Response
    {
        try {
            $department = Department::findOrFail($id);
            $department->update($request->all());
            $response = new EditDepartmentResource($department);
            return response([
                'message' => 'Department successfully updated!',
                'department' => $response,
            ]);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 0:
                    return response([
                        'message' => 'A department not found!'
                    ], 404);
                default:
                    return response([
                        'message' => $e->getCode()
                    ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $department = Department::findOrFail($id);
            $department->delete();
            return response([
                'message' => 'Department successfully deleted!'
            ]);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case '23000':
                    return response([
                        'message' => 'A department cannot be deleted while it has employees!'
                    ], 500);
                case 0:
                    return response([
                        'message' => 'A department not found!'
                    ], 404);
                default:
                    return response([
                        'message' => $e->getCode()
                    ], 500);
            }
        }
    }
}
