<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Resources\EditEmployeeResource;
use App\Http\Resources\EmployeeCollection;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request  $request): Response
    {
        $employees = Employee::orderBy('id', 'desc')->paginate($request->pageSize);
        $response = new EmployeeCollection($employees);
        return response($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreEmployeeRequest  $request
     * @return Response
     */
    public function store(StoreEmployeeRequest $request): Response
    {
        $employee = Employee::create($request->all());
        $employee->departments()->sync($request->departments_ids);
        return response([
            'message' => 'Employee successfully added!',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id): Response
    {
        $employee = Employee::findOrFail($id);
        $response = new EditEmployeeResource($employee);
        return response($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreEmployeeRequest $request
     * @param int $id
     * @return Response
     */
    public function update(StoreEmployeeRequest $request, int $id): Response
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->update($request->all());
            $employee->departments()->sync($request->departments_ids);
            $response = new EditEmployeeResource($employee);
            return response([
                'message' => 'Employee successfully updated!',
                'employee' => $response,
            ]);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 0:
                    return response([
                        'message' => 'A employee not found!'
                    ], 404);
                default:
                    return response([
                        'message' => $e->getCode()
                    ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->delete();
            return response([
                'message' => 'Employee successfully deleted!'
            ]);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 0:
                    return response([
                        'message' => 'A employee not found!'
                    ], 404);
                default:
                    return response([
                        'message' => $e->getCode()
                    ], 500);
            }
        }
    }
}
