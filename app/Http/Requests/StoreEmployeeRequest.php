<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'second_name' => 'required|min:3|max:255',
            'salary' => 'required|integer',
            'departments_ids' => 'required|array'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'first_name.required' => 'A first name field is required! Minimum number of characters 3, maximum 255',
            'last_name.required' => 'A last name field is required! Minimum number of characters 3, maximum 255',
            'second_name.required' => 'A second name field is required! Minimum number of characters 3, maximum 255',
            'salary.required' => 'A salary is required! Only integer can be entered',
        ];
    }
}
