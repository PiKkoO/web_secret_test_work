<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentEmployee extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The DB table name.
     *
     * @var string
     */
    protected $table = 'department_employee';
}
