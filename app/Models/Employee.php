<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Employee extends Model
{
    use HasFactory;

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

    /**
     * The DB table name.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'second_name',
        'gender',
        'salary',
    ];

    /**
     * adding the appends value will call the accessor in the JSON response.
     *
     * @var array
     */

    protected $appends = ['departments_ids'];

    /**
     * The departments that belong to the employee.
     */
    public function departments(): BelongsToMany
    {
        return $this->belongsToMany(Department::class, 'department_employee');
    }

    /**
     * Get departments ids.
     */

    public function getDepartmentsIdsAttribute()
    {
        return $this->departments->pluck('id');
    }
}
