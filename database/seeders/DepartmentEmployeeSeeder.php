<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DepartmentEmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DepartmentEmployee::factory(100)->create();
    }
}
