import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import "antd/dist/antd.css";
import Home from "./home/Home";
import Departments from "./departments/Departments";
import Department from "./departments/Department";
import Employees from "./employees/Employees";
import Employee from "./employees/Employee";
import {applyMiddleware, compose, createStore} from "redux";
import thunk from 'redux-thunk';
import { Provider } from "react-redux";
import { rootReducer } from "../redux/rootReducer";

const store = createStore(rootReducer, compose(
    applyMiddleware(
        thunk
    ),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

const app = (
    <Provider store={store}>
        <App />
    </Provider>
)

function App() {

    return (
        <Router>
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/departments">Departments</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/employees">Employees</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <Switch>
                    <Route path="/departments/edit/:id">
                        <Department />
                    </Route>
                    <Route path="/departments">
                        <Departments />
                    </Route>
                    <Route path="/employees/edit/:id">
                        <Employee />
                    </Route>
                    <Route path="/employees">
                        <Employees />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(app, document.getElementById('root'));
}
