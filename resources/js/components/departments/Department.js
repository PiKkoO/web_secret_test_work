import React, { useEffect } from 'react';
import {Form, Input, Button, Select} from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchDepartment, updateDepartment } from "../../redux/departments/actions";

function Department () {
    const { selectedDepartment } = useSelector(
        state => state.departments
    );
    const dispatch = useDispatch()

    let { id } = useParams();

    const [ form ] = Form.useForm();

    useEffect(() => {
        dispatch(fetchDepartment(id))
    }, [dispatch])

    useEffect(() => {
        if (selectedDepartment)
            form.setFieldsValue(selectedDepartment);
    }, [selectedDepartment])

    const onFinish = (values) => {
        // console.log('Success:', values)
        dispatch(updateDepartment(id, values))
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo)
    };

    return (
        <div className={"container mt-5 mb-5"}>
            <h2 className={"text-center mb-5"}>{ selectedDepartment.name }</h2>
            <Form
                form={form}
                name="update_department"
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Department Name"
                    name="name"
                    rules={[{ required: true, message: 'Please input department name!', min: 3, max: 255 }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
                    <Button className={"float-right"} type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Department
