import React, { useEffect } from 'react';
import { Form, Input, Table, Space, Button, Modal } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import { fetchDepartments, createDepartment, handlePaginationChange, setModalVisible, deleteDepartment } from "../../redux/departments/actions";
import { ExclamationCircleOutlined } from "@ant-design/icons";

const { confirm } = Modal;

function Departments() {
    const { departments, pagination, loading, modal_visibility } = useSelector(
        state => state.departments
    );
    const dispatch = useDispatch()

    const params = {
        page: pagination.current,
        pageSize: pagination.pageSize
    }

    useEffect(() => {
        dispatch(fetchDepartments(params))
    }, [dispatch])

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Total employees',
            dataIndex: 'employees_count',
            key: 'employees_count',
        },
        {
            title: 'Max salary',
            dataIndex: 'max_salary',
            key: 'max_salary',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Button href={`/departments/edit/${record.id}`} type={"primary"}>Edit</Button>
                    <Button type={"primary"} onClick={() => showDeleteConfirm(record.id)} danger={true}>Delete</Button>
                </Space>
            ),
        },
    ];

    const onPaginationChange = (page, pageSize) => {
        dispatch(handlePaginationChange(page, pageSize))
        dispatch(fetchDepartments({
            page: page.current,
            pageSize: page.pageSize,
        }))
    }

    const showModal = () => {
        dispatch(setModalVisible(true))
    }

    const hideModal = () => {
        dispatch(setModalVisible(false))
    }

    const onFinish = (values) => {
        dispatch(createDepartment(values, params))
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo)
    };

    const showDeleteConfirm = (id = 0) => {
        confirm({
            title: 'Are you sure delete this record from data base?',
            icon: <ExclamationCircleOutlined />,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                dispatch(deleteDepartment(id, params))
            },
        });
    }

    return (
        <div className={"container mt-5 mb-5"}>
            <h2 className={"text-center mb-5"}>Departments</h2>
            <Button className={"mt-3 mb-3 float-right"} type="primary" onClick={showModal}>
                Add department
            </Button>
            <Modal title="Enter data" visible={modal_visibility}
                   onCancel={hideModal}
                   footer={null}
            >
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Department name"
                        name="name"
                        rules={[{ required: true, message: 'Please input your department name!', min: 3, max: 255 }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }} style={{ marginBottom: 0 }}>
                        <Button className={"float-right"} type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
            <Table
                rowKey={record => record.id}
                columns={columns}
                dataSource={departments}
                loading={loading}
                pagination={pagination}
                onChange={onPaginationChange}
            />
        </div>
    );
}

export default Departments;
