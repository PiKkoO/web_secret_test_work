import React, { useEffect } from 'react';
import { Form, Input, Button, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { fetchDepartments, fetchEmployee, updateEmployee } from "../../redux/employees/actions";
import { useParams } from "react-router-dom";

const { Option } = Select;

function Employee () {
    const { selectedEmployee, departments } = useSelector(
        state => state.employees
    );
    const dispatch = useDispatch()

    let { id } = useParams();

    const [ form ] = Form.useForm();

    useEffect(() => {
        dispatch(fetchEmployee(id))
        dispatch(fetchDepartments())
    }, [dispatch])

    useEffect(() => {
        if (selectedEmployee)
            form.setFieldsValue(selectedEmployee)
    }, [selectedEmployee])

    const onFinish = (values) => {
        dispatch(updateEmployee(id, values))

    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo)
    };

    const options = departments.map((value) => {
        let data = {}
        data = {
            ...data,
            value: value.id,
            label: value.name
        }
        return data
    })

    return (
        <div className={"container mt-5 mb-5"}>
            <h2 className={"text-center mb-5"}>{ selectedEmployee.last_name } { selectedEmployee.first_name } { selectedEmployee.second_name }</h2>
            <Form
                form={form}
                name="update_employee"
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="First Name"
                    name="first_name"
                    rules={[{ required: true, message: 'Please input your first name!', min: 3, max: 255 }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Last Name"
                    name="last_name"
                    rules={[{ required: true, message: 'Please input your last name!', min: 3, max: 255 }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Second Name"
                    name="second_name"
                    rules={[{ required: true, message: 'Please input your second name!', min: 3, max: 255 }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Gender"
                    name="gender"
                >
                    <Select>
                        <Option value="male">Male</Option>
                        <Option value="female">Female</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    label="Salary"
                    name="salary"
                    rules={[{
                        required: true,
                        message: 'Please input correct salary!',
                        pattern: /^[0-9]+$/,
                    }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Departments"
                    name="departments_ids"
                    rules={[{ required: true, message: 'Please select department(s)!'}]}
                >
                    <Select
                        mode="multiple"
                        options={options}
                    >
                    </Select>
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
                    <Button className={"float-right"} type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Employee;
