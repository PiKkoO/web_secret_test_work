import React, { useEffect } from 'react';
import { Form, Input, Table, Space, Button, Modal, Select } from "antd";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from "react-redux";
import { fetchEmployees, fetchDepartments, handlePaginationChange, createEmployee, setAddModalVisible, deleteEmployee } from "../../redux/employees/actions";

const { Option } = Select;
const { confirm } = Modal;

function Employees() {
    const { employees, departments, pagination, loading, add_modal_visibility } = useSelector(
        state => state.employees
    );
    const dispatch = useDispatch()

    const [ form ] = Form.useForm();

    const params = {
        page: pagination.current,
        pageSize: pagination.pageSize
    }

    useEffect(() => {
        dispatch(fetchEmployees(params))
        dispatch(fetchDepartments())
    }, [dispatch])

    const columns = [
        {
            title: 'Full Name',
            dataIndex: 'full_name',
            key: 'full_name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Gender',
            dataIndex: 'gender',
            key: 'gender',
        },
        {
            title: 'Salary',
            dataIndex: 'salary',
            key: 'salary',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Button href={`/employees/edit/${record.id}`} type={"primary"}>Edit</Button>
                    <Button type={"primary"} onClick={() => showDeleteConfirm(record.id)} danger={true}>Delete</Button>
                </Space>
            ),
        },
    ];

    const onPaginationChange = (page, pageSize) => {
        dispatch(handlePaginationChange(page, pageSize))
        dispatch(fetchEmployees({
            page: page.current,
            pageSize: page.pageSize,
        }))
    }

    const showDeleteConfirm = (id = 0) => {
        confirm({
            title: 'Are you sure delete this record from data base?',
            icon: <ExclamationCircleOutlined />,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                dispatch(deleteEmployee(id, params))
            },
        });
    }

    const showAddModal = () => {
        dispatch(setAddModalVisible(true))
    }

    const hideAddModal = () => {
        dispatch(setAddModalVisible(false))
    }

    const onFinish = (values) => {
        dispatch(createEmployee(values, params))
        form.resetFields();
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo)
    };

    const options = departments.map((value) => {
        let data = {}
        data = {
            ...data,
            value: value.id,
            label: value.name
        }
        return data
    })

    return (
        <div className={"container mt-5 mb-5"}>
            <h2 className={"text-center mb-5"}>Employees</h2>
            <Button className={"mt-3 mb-3 float-right"} type="primary" onClick={showAddModal}>
                Add employee
            </Button>
            <Modal title="Enter data"
                   visible={add_modal_visibility}
                   onCancel={hideAddModal}
                   footer={null}
            >
                <Form
                    form={form}
                    name="add_employee"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ gender: 'male' }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="First Name"
                        name="first_name"
                        rules={[{ required: true, message: 'Please input your first name!', min: 3, max: 255 }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Last Name"
                        name="last_name"
                        rules={[{ required: true, message: 'Please input your last name!', min: 3, max: 255 }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Second Name"
                        name="second_name"
                        rules={[{ required: true, message: 'Please input your second name!', min: 3, max: 255 }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Gender"
                        name="gender"
                    >
                        <Select>
                            <Option value="male">Male</Option>
                            <Option value="female">Female</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label="Salary"
                        name="salary"
                        rules={[{
                            required: true,
                            message: 'Please input correct salary!',
                            pattern: /^[0-9]+$/,
                        }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Departments"
                        name="departments_ids"
                        rules={[{ required: true, message: 'Please select department(s)!'}]}
                    >
                        <Select
                            mode="multiple"
                            options={options}
                        >
                        </Select>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }} style={{ marginBottom: 0 }}>
                        <Button className={"float-right"} type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
            <Table
                rowKey={record => record.id}
                columns={columns}
                dataSource={employees}
                loading={loading}
                pagination={pagination}
                onChange={onPaginationChange}
            />
        </div>
    );
}

export default Employees;
