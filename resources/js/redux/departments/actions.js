import {
    FETCH_DEPARTMENTS,
    FETCH_DEPARTMENT,
    HANDLE_PAGINATION_CHANGE,
    LOADING_TRUE,
    LOADING_FALSE,
    SET_MODAL
} from "./types";
import reqwest from 'reqwest';
import { message } from "antd";

export function loadingTrue () {
    return {
        type: LOADING_TRUE,
        payload: true
    }
}

export function loadingFalse () {
    return {
        type: LOADING_FALSE,
        payload: false
    }
}

export function setModalVisible (value) {
    return {
        type: SET_MODAL,
        payload: value
    }
}

export function handlePaginationChange (page, pageSize) {
    return {
        type: HANDLE_PAGINATION_CHANGE,
        payload: { page, pageSize }
    }
}

export function fetchDepartments (params = {}) {
    let string_params = new URLSearchParams(params).toString()
    return async dispatch => {
        try {
            dispatch(loadingTrue())
            await reqwest({
                url: '/api/departments/all',
                method: 'get',
                type: 'json',
                data: string_params
            }).then(response => {
                dispatch({
                    type: FETCH_DEPARTMENTS,
                    payload: response
                })
                dispatch(loadingFalse())
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function fetchDepartment (id = 0) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/departments/edit/' + id,
                method: 'get',
                type: 'json',
            }).then(response => {
                dispatch({
                    type: FETCH_DEPARTMENT,
                    payload: response
                })
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function createDepartment (values = {}, params = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/departments/store',
                method: 'post',
                type: 'json',
                data: values
            }).then(response => {
                message.success(response.message)
                dispatch(loadingTrue())
                dispatch(fetchDepartments(params))
                dispatch(loadingFalse())
                dispatch(setModalVisible(false))
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function updateDepartment (id = 0, values = {}, params = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/departments/update/'+ id,
                method: 'put',
                type: 'json',
                data: values
            }).then(response => {
                message.success(response.message)
                dispatch({
                    type: FETCH_DEPARTMENT,
                    payload: response.department
                })
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function deleteDepartment (id = 0, params = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/departments/destroy/'+ id,
                method: 'delete',
                type: 'json',
            }).then(response => {
                message.success(response.message)
                dispatch(loadingTrue())
                dispatch(fetchDepartments(params))
                dispatch(loadingFalse())
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}
