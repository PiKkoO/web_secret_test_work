import {
    FETCH_DEPARTMENTS,
    FETCH_DEPARTMENT,
    HANDLE_PAGINATION_CHANGE,
    LOADING_FALSE,
    LOADING_TRUE,
    SET_MODAL
} from "./types";

const initialState = {
    departments: [],
    selectedDepartment: {},
    pagination: {
        current: 1,
        pageSize: 5,
        showSizeChanger: true,
        pageSizeOptions: [5, 10, 20, 50],
    },
    loading: false,
    modal_visibility: false,
}

export const departmentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DEPARTMENTS:
            return {
                ...state,
                departments: action.payload.data,
                pagination: {
                    ...state.pagination,
                    total: action.payload.meta.total,
                },
            }
        case FETCH_DEPARTMENT:
            return {
                ...state,
                selectedDepartment: action.payload,
            }
        case HANDLE_PAGINATION_CHANGE:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    current: action.payload.page.current,
                    pageSize: action.payload.page.pageSize,
                }
            }
        case LOADING_TRUE:
            return {
                ...state,
                loading: action.payload

            }
        case LOADING_FALSE:
            return {
                ...state,
                loading: action.payload
            }
        case SET_MODAL:
            return {
                ...state,
                modal_visibility: action.payload
            }
        default: return state
    }
}
