import {
    FETCH_EMPLOYEES,
    HANDLE_PAGINATION_CHANGE,
    LOADING_TRUE, LOADING_FALSE,
    SET_ADD_MODAL,
    FETCH_DEPARTMENTS,
    FETCH_EMPLOYEE
} from "./types";
import reqwest from 'reqwest';
import { message } from "antd";

export function loadingTrue () {
    return {
        type: LOADING_TRUE,
        payload: true
    }
}

export function loadingFalse () {
    return {
        type: LOADING_FALSE,
        payload: false
    }
}

export function setAddModalVisible (value) {
    return {
        type: SET_ADD_MODAL,
        payload: value
    }
}

export function handlePaginationChange (page, pageSize) {
    return {
        type: HANDLE_PAGINATION_CHANGE,
        payload: { page, pageSize }
    }
}

export function fetchEmployees (params = {}) {
    let string_params = new URLSearchParams(params).toString()
    return async dispatch => {
        try {
            dispatch(loadingTrue())
            await reqwest({
                url: '/api/employees/all',
                method: 'get',
                type: 'json',
                data: string_params
            }).then(response => {
                dispatch({
                    type: FETCH_EMPLOYEES,
                    payload: response
                })
                dispatch(loadingFalse())
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function fetchEmployee (id = 0) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/employees/edit/' + id,
                method: 'get',
                type: 'json',
            }).then(response => {
                dispatch({
                    type: FETCH_EMPLOYEE,
                    payload: response
                })
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function fetchDepartments () {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/departments/all',
                method: 'get',
                type: 'json',
            }).then(response => {
                dispatch({
                    type: FETCH_DEPARTMENTS,
                    payload: response
                })
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function createEmployee (values = {}, params = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/employees/store',
                method: 'post',
                type: 'json',
                data: values
            }).then(response => {
                message.success(response.message)
                dispatch(loadingTrue())
                dispatch(fetchEmployees(params))
                dispatch(loadingFalse())
                dispatch(setAddModalVisible(false))
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }

    }
}

export function updateEmployee (id = 0, values = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/employees/update/'+ id,
                method: 'put',
                type: 'json',
                data: values
            }).then(response => {
                message.success(response.message)
                dispatch({
                    type: FETCH_EMPLOYEE,
                    payload: response.employee
                })
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}

export function deleteEmployee (id = 0, params = {}) {
    return async dispatch => {
        try {
            await reqwest({
                url: '/api/employees/destroy/'+ id,
                method: 'delete',
                type: 'json',
            }).then(response => {
                message.success(response.message)
                dispatch(loadingTrue())
                dispatch(fetchEmployees(params))
                dispatch(loadingFalse())
            })
        }catch (error) {
            message.error(JSON.parse(error.response).message)
        }
    }
}
