import {
    FETCH_EMPLOYEES,
    HANDLE_PAGINATION_CHANGE,
    LOADING_FALSE,
    LOADING_TRUE,
    SET_ADD_MODAL,
    FETCH_DEPARTMENTS,
    FETCH_EMPLOYEE
} from "./types";

const initialState = {
    employees: [],
    selectedEmployee: {},
    departments: [],
    pagination: {
        current: 1,
        pageSize: 5,
        showSizeChanger: true,
        pageSizeOptions: [5, 10, 20, 50],
    },
    loading: false,
    add_modal_visibility: false,
}

export const employeesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_EMPLOYEES:
            return {
                ...state,
                employees: action.payload.data,
                pagination: {
                    ...state.pagination,
                    total: action.payload.meta.total,
                },
            }
        case FETCH_EMPLOYEE:
            return {
                ...state,
                selectedEmployee: action.payload,
            }
        case FETCH_DEPARTMENTS:
            return {
                ...state,
                departments: action.payload,
            }
        case HANDLE_PAGINATION_CHANGE:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    current: action.payload.page.current,
                    pageSize: action.payload.page.pageSize,
                }
            }
        case LOADING_TRUE:
            return {
                ...state,
                loading: action.payload

            }
        case LOADING_FALSE:
            return {
                ...state,
                loading: action.payload
            }
        case SET_ADD_MODAL:
            return {
                ...state,
                add_modal_visibility: action.payload
            }
        default: return state
    }
}
