import { combineReducers } from "redux";
import { departmentsReducer } from "./departments/reducer";
import { employeesReducer } from "./employees/reducer";

export const rootReducer = combineReducers({
    departments: departmentsReducer,
    employees: employeesReducer,
})
