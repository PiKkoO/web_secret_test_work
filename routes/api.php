<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Departments\DepartmentController;
use App\Http\Controllers\Employees\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Departments')->prefix('/departments')->group(function () {
    Route::post('/store', [DepartmentController::class, 'store'])->name('department.store');
    Route::get('/all', [DepartmentController::class, 'index'])->name('department.index');
    Route::get('/edit/{id}', [DepartmentController::class, 'edit'])->name('department.edit');
    Route::put('/update/{id}', [DepartmentController::class, 'update'])->name('department.update');
    Route::delete('/destroy/{id}', [DepartmentController::class, 'destroy'])->name('department.destroy');
});

Route::namespace('Employees')->prefix('/employees')->group(function () {
    Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
    Route::get('/all', [EmployeeController::class, 'index'])->name('employee.index');
    Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
    Route::put('/update/{id}', [EmployeeController::class, 'update'])->name('employee.update');
    Route::delete('/destroy/{id}', [EmployeeController::class, 'destroy'])->name('employee.destroy');
});
